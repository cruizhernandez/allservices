#!/bin/bash
clear
read -p 'YOU WANT INSTALL OR UNISTALL? i/u: ' install
case $install in
'')
exit
;;
i|install|instalar)
clear;
echo 'This configuration corresponds to a dynamic dns'; echo 'make sure you run this script as an administrator'; read -p 'Press any key+ENTER to continue: ' foo_var;echo;
clear
read -p 'the DDNS that is to be installed is Bind/ISC. do they agree?yes/no: ' answer
#CASEFORINSTALL
case $answer in
'') clear;exit
;;
yes|y|si|s)clear;
apt-get -y install bind9 && apt-get -y install isc-dhcp-server;
;;
no|n)exit
;;
*)exit
;;
esac

#COLLECTINFO
clear;
read -p 'Enter the name of the direct zone (WITHOUT DOT AN END): ' directzone;echo
echo 'Enter the complete name of the inverse zone, '; read -p 'eg: 10.168.192.in-addr.arpa :  ' inversezone
echo;read -p 'Please ENTER the DNS IP server: ' ipserver
echo;read -p 'Please ENTER the DHCP IP server: ' dhcpserver
#NAMED.CONF.LOCAL
echo
cp /etc/bind/db.local /var/lib/bind/db.$directzone; cp /etc/bind/db.127 /var/lib/bind/db.$inversezone
echo '' >> /etc/bind/named.conf.local
echo "zone $directzone {" >> /etc/bind/named.conf.local
echo '	type master;' >> /etc/bind/named.conf.local
echo "	file \"/var/lib/bind/db.$directzone\";" >> /etc/bind/named.conf.local
echo "  allow-update {$dhcpserver;};" >> /etc/bind/named.conf.local
echo "  notify yes;" >> /etc/bind/named.conf.local
echo '	};' >> /etc/bind/named.conf.local
echo '' >> /etc/bind/named.conf.local
echo "zone $inversezone { " >> /etc/bind/named.conf.local
echo '	type master;' >> /etc/bind/named.conf.local
echo "	file \"/var/lib/bind/db.$inversezone\";" >> /etc/bind/named.conf.local
echo "  allow-update {$dhcpserver;};" >> /etc/bind/named.conf.local
echo "  notify yes;" >> /etc/bind/named.conf.local
echo '	};' >> /etc/bind/named.conf.local

#DIRECTZONE
echo ';' > /var/lib/bind/db.$directzone
echo '; BIND data file for local loopback interface' >> /var/lib/bind/db.$directzone
echo ';' >> /var/lib/bind/db.$directzone
echo '$TTL	604800' >> /var/lib/bind/db.$directzone
echo "\$ORIGIN $directzone." >> /var/lib/bind/db.$directzone
echo "@	IN	SOA	$HOSTNAME.$directzone. root.$HOSTNAME.$directzone. (" >> /var/lib/bind/db.$directzone
echo '			      2		; Serial' >> /var/lib/bind/db.$directzone
echo '			 604800		; Refresh' >> /var/lib/bind/db.$directzone
echo '			  86400		; Retry' >> /var/lib/bind/db.$directzone
echo '			2419200		; Expire' >> /var/lib/bind/db.$directzone
echo '			 604800 )	; Negative Cache TTL' >> /var/lib/bind/db.$directzone
echo ';' >> /var/lib/bind/db.$directzone
echo "	NS	$HOSTNAME.$directzone." >> /var/lib/bind/db.$directzone
echo "$HOSTNAME	A	$ipserver" >> /var/lib/bind/db.$directzone


#REVERSEZONE
echo ';' > /var/lib/bind/db.$inversezone
echo '; BIND reverse data file for local loopback interface' >> /var/lib/bind/db.$inversezone
echo ';' >> /var/lib/bind/db.$inversezone
echo '$TTL	604800' >> /var/lib/bind/db.$inversezone
echo "\$ORIGIN $inversezone." >> /var/lib/bind/db.$inversezone
echo "@	IN	SOA	$HOSTNAME.$directzone. root.$HOSTNAME.$directzone. (" >> /var/lib/bind/db.$inversezone
echo ' 			      1		; Serial' >> /var/lib/bind/db.$inversezone;echo '			 604800		; Refresh' >> /var/lib/bind/db.$inversezone;echo '			  86400		; Retry' >> /var/lib/bind/db.$inversezone;echo '			2419200		; Expire' >> /var/lib/bind/db.$inversezone;echo '			 604800 )	; Negative Cache TTL' >> /var/lib/bind/db.$inversezone;echo ';' >> /var/lib/bind/db.$inversezone
echo "	IN	NS	$HOSTNAME.$directzone." >> /var/lib/bind/db.$inversezone

echo "$ipserver" > test.tmp
id=$(cat test.tmp | cut -d "." -f 4)
echo "$id	IN	PTR	$HOSTNAME.$directzone." >> /var/lib/bind/db.$inversezone 
rm -rf test.tmp

#DHCP
cat commentsdhcp1 > /etc/dhcp/dhcpd.conf
echo
read -p "Please Enter your network id. eg: 192.168.10.0: " idnet
echo
read -p "Please Enter your netmask. eg: 255.255.255.0: " netmask
echo
read -p "Please Enter the Broadcast-address. eg: 255.255.255.255: " broadcast
echo
read -p "Please Enter the INITIAL ip of the DHCP range: " initial
echo
read -p "Please Enter the FINAL ip of the DHCP range: " final
echo
read -p "Please Enter the Gateway address: " gateway
echo
read -p "Please Enter NIC name. eg: eth0: " interface
sed -in "s/^INTERFACES.*/INTERFACES\=\"$interface\"/" /etc/default/isc-dhcp-server
#DHCPD.CONF
echo "server-identifier $dhcpserver;" > /etc/dhcp/dhcpd.conf
echo "ddns-updates on;" >> /etc/dhcp/dhcpd.conf
echo "ddns-update-style interim;" >> /etc/dhcp/dhcpd.conf
echo "ddns-domainname \"$directzone\";" >> /etc/dhcp/dhcpd.conf
echo "ddns-rev-domainname \"in-addr.arpa\";" >> /etc/dhcp/dhcpd.conf
echo "ignore client-updates;" >> /etc/dhcp/dhcpd.conf
echo "	zone $directzone {" >> /etc/dhcp/dhcpd.conf
echo "	primary $ipserver;" >> /etc/dhcp/dhcpd.conf
echo "	}" >> /etc/dhcp/dhcpd.conf
echo "option domain-name \"$directzone\";" >> /etc/dhcp/dhcpd.conf
echo "option domain-name-servers $ipserver;" >> /etc/dhcp/dhcpd.conf
echo "option ip-forwarding off;" >> /etc/dhcp/dhcpd.conf
echo "default-lease-time 600;" >> /etc/dhcp/dhcpd.conf
echo "max-lease-time 7200;" >> /etc/dhcp/dhcpd.conf
echo "authoritative;" >> /etc/dhcp/dhcpd.conf
echo "log-facility local7;" >> /etc/dhcp/dhcpd.conf
echo "" >> /etc/dhcp/dhcpd.conf
echo "# A slightly different configuration for an internal subnet." >> /etc/dhcp/dhcpd.conf
echo "subnet $idnet netmask $netmask {" >> /etc/dhcp/dhcpd.conf
echo "  range $initial $final;" >> /etc/dhcp/dhcpd.conf
echo "  option domain-name-servers $ipserver;" >> /etc/dhcp/dhcpd.conf
echo "  option domain-name \"$directzone\";" >> /etc/dhcp/dhcpd.conf
echo "  option routers $gateway;" >> /etc/dhcp/dhcpd.conf
echo "  option broadcast-address $broadcast;" >> /etc/dhcp/dhcpd.conf
echo "  allow unknown-clients;" >> /etc/dhcp/dhcpd.conf
echo "	zone $directzone {" >> /etc/dhcp/dhcpd.conf
echo "	primary $ipserver;" >> /etc/dhcp/dhcpd.conf
echo "	}" >> /etc/dhcp/dhcpd.conf
echo "	zone $inversezone {" >> /etc/dhcp/dhcpd.conf
echo "	primary $ipserver;" >> /etc/dhcp/dhcpd.conf
echo "	}" >> /etc/dhcp/dhcpd.conf
echo "}" >> /etc/dhcp/dhcpd.conf

#RESERVE
clear
read -p 'You need reserve ip for some host? y/n: ' fantasia
#FANTASIA
case $fantasia in
'')
cat commentsdhcp2 >> /etc/dhcp/dhcpd.conf
;;
yes|y|si|s)
clear
read -p "How many host wants to make a reservation? " nums
while [ $nums -gt 0 ]
do
echo
read -p "Please Enter the hostname: " host
echo
read -p "Please Enter the mac-address. eg; 08:00:07:26:c0:a5: " mac
echo
read -p "Please Enter the ip for the reservation: " ipre
echo "host $host.$directzone {" >> /etc/dhcp/dhcpd.conf
echo "hardware ethernet $mac;" >> /etc/dhcp/dhcpd.conf
echo "fixed-address $ipre;" >> /etc/dhcp/dhcpd.conf
echo "}" >> /etc/dhcp/dhcpd.conf
let nums-=1
echo
echo "$nums left"
done
cat commentsdhcp2 >> /etc/dhcp/dhcpd.conf
;;
no|n)
cat commentsdhcp2 >> /etc/dhcp/dhcpd.conf
;;
*)
cat commentsdhcp2 >> /etc/dhcp/dhcpd.conf
;;
esac


#finish
service isc-dhcp-server restart
service bind9 restart
clear
echo "The DDNS is already installed"
echo "Remember to check the IP in you network interface"
echo
echo 'IMPORTANT: If you this virtualizing remember change the network interface'
echo 'from NAT to some local and them RESTART the services bind9 and isc-dhcp-server'
echo "and your /etc/resolv.conf"
echo "If you need further zones should enter them by hand"
echo
echo "Script by: Cristhian Ruiz Hernández TET03 2014/2016"
;;
u|unistall|desinstalar)
service isc-dhcp-server stop
rm -rf /etc/dhcp
rm -rf /var/lib/bind/db.*
rm -rf /etc/bind
sed -in "s/^INTERFACES.*/INTERFACES\=\"\"/" /etc/default/isc-dhcp-server
apt-get remove --purge isc-dhcp-server bind9

;;
*)
exit
;;
esac
