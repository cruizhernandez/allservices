#!/bin/bash
clear
read -p 'YOU WANT INSTALL OR UNISTALL? i/u: ' ins

case $ins in
'')
exit
;;
i|insta|instalar)
clear;
echo 'This configuration corresponds to a static dns'; echo 'make sure you run this script as an administrator'; read -p 'Press any key+ENTER to continue: ' foo_var;echo;
clear
	read -p 'the DNS that is to be installed is Bind. do they agree?yes/no: ' answer
	#CASEFORINSTALL
	case $answer in
	'') clear;exit
	;;
	yes|y|si|s)clear;
	apt-get -y install bind9;
	;;
	no|n)exit
	;;
	*)exit
	;;
	esac

#COLLECTINFO
clear;
read -p 'Enter the name of the direct zone (WITHOUT DOT AN END): ' directzone;echo
echo 'Enter the complete name of the inverse zone, '; read -p 'eg: 10.168.192.in-addr.arpa :  ' inversezone
echo;read -p 'Please ENTER the DNS IP server: ' ipserver

#NAMED.CONF.LOCAL
echo
cp /etc/bind/db.local /var/lib/bind/db.$directzone; cp /etc/bind/db.127 /var/lib/bind/db.$inversezone
echo '' >> /etc/bind/named.conf.local
echo "zone $directzone {" >> /etc/bind/named.conf.local
echo '	type master;' >> /etc/bind/named.conf.local
echo "	file \"/var/lib/bind/db.$directzone\";" >> /etc/bind/named.conf.local
echo '	};' >> /etc/bind/named.conf.local
echo '' >> /etc/bind/named.conf.local
echo "zone $inversezone { " >> /etc/bind/named.conf.local
echo '	type master;' >> /etc/bind/named.conf.local
echo "	file \"/var/lib/bind/db.$inversezone\";" >> /etc/bind/named.conf.local
echo '	};' >> /etc/bind/named.conf.local

#DIRECTZONE
echo ';' > /var/lib/bind/db.$directzone
echo '; BIND data file for local loopback interface' >> /var/lib/bind/db.$directzone
echo ';' >> /var/lib/bind/db.$directzone
echo '$TTL	604800' >> /var/lib/bind/db.$directzone
echo "\$ORIGIN $directzone." >> /var/lib/bind/db.$directzone
echo "@	IN	SOA	$HOSTNAME.$directzone. root.$HOSTNAME.$directzone. (" >> /var/lib/bind/db.$directzone
echo '			      2		; Serial' >> /var/lib/bind/db.$directzone
echo '			 604800		; Refresh' >> /var/lib/bind/db.$directzone
echo '			  86400		; Retry' >> /var/lib/bind/db.$directzone
echo '			2419200		; Expire' >> /var/lib/bind/db.$directzone
echo '			 604800 )	; Negative Cache TTL' >> /var/lib/bind/db.$directzone
echo ';' >> /var/lib/bind/db.$directzone
echo "	NS	$HOSTNAME.$directzone." >> /var/lib/bind/db.$directzone
echo "$HOSTNAME	A	$ipserver" >> /var/lib/bind/db.$directzone


#REVERSEZONE
echo ';' > /var/lib/bind/db.$inversezone
echo '; BIND reverse data file for local loopback interface' >> /var/lib/bind/db.$inversezone
echo ';' >> /var/lib/bind/db.$inversezone
echo '$TTL	604800' >> /var/lib/bind/db.$inversezone
echo "\$ORIGIN $inversezone." >> /var/lib/bind/db.$inversezone
echo "@	IN	SOA	$HOSTNAME.$directzone. root.$HOSTNAME.$directzone. (" >> /var/lib/bind/db.$inversezone
echo ' 			      1		; Serial' >> /var/lib/bind/db.$inversezone;echo '			 604800		; Refresh' >> /var/lib/bind/db.$inversezone;echo '			  86400		; Retry' >> /var/lib/bind/db.$inversezone;echo '			2419200		; Expire' >> /var/lib/bind/db.$inversezone;echo '			 604800 )	; Negative Cache TTL' >> /var/lib/bind/db.$inversezone;echo ';' >> /var/lib/bind/db.$inversezone
echo "	IN	NS	$HOSTNAME.$directzone." >> /var/lib/bind/db.$inversezone
echo "$ipserver" > test.tmp
id=$(cat test.tmp | cut -d "." -f 4)
echo "$id	IN	PTR	$HOSTNAME.$directzone." >> /var/lib/bind/db.$inversezone 
rm -rf test.tmp
#RECORDS
clear;echo 'the RECORD of the server was added successfully in the direct and reverse zone'
read -p 'How many RECORDS/CLIENTS you need add: ' numclients
while [ $numclients -gt 0 ]
do
read -p 'Please Enter the hostname of the client: ' client
read -p 'Please Enter the IP of the client: ' ip
echo "$client	IN	A	$ip" >> /var/lib/bind/db.$directzone
echo "$ip	IN	PTR	$client\.$directzone\." >> /var/lib/bind/db.$inversezone
let numclients-=1
echo;echo "$client with the ip $ip was addes successfully"; echo "$numclients left to add";echo
done
clear

#finish
service bind9 restart
clear
echo "The DNS is already installed"
echo "Remember to check the IP in you network interface"
echo "and your /etc/resolv.conf"
echo "If you need further zones should enter them by hand"
echo
echo "Script by: Cristhian Ruiz Hernández TET03 2014/2016"
;;

u|unistall)
clear
service bind9 stop
rm -rf /var/lib/bind/db.*
rm -rf /etc/bind
apt-get remove --purge bind9
exit
;;
*)
exit
;;
esac
