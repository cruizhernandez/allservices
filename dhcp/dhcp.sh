#!/bin/bash
clear
read -p 'YOU WANT INSTALL OR UNISTALL? i/u: ' install
case $install in
'')
exit
;;
i|install|instalar)
clear;
echo 'This configuration corresponds to a dhcp'; echo 'make sure you run this script as an administrator'; read -p 'Press any key+ENTER to continue: ' foo_var;echo;
clear
echo 'the DHCP that is to be installed is isc-dhcp-server.'
read -p 'do they agree?yes/no: ' answer

	#CASEFORINSTALL
	case $answer in
	'') clear;exit
	;;
	yes|y|si|s)clear;
	apt-get -y install isc-dhcp-server;
	;;
	no|n)exit
	;;
	*)exit
	;;
	esac
	
cat commentsdhcp1 > /etc/dhcp/dhcpd.conf
clear
read -p "Please Enter your network id. eg: 192.168.10.0: " idnet
echo
read -p "Please Enter your netmask. eg: 255.255.255.0: " netmask
echo
read -p "Please Enter the Broadcast-address. eg: 255.255.255.255: " broadcast
echo
read -p "Please Enter the INITIAL ip of the DHCP range: " initial
echo
read -p "Please Enter the FINAL ip of the DHCP range: " final
echo
read -p "Please Enter the DNS ip address: " dnsip
echo
read -p "Please Enter the Gateway address: " gateway
echo
read -p "Please Enter domain name. eg: ina.net: " domain
echo
read -p "Please Enter NIC name. eg: eth0: " interface
sed -in "s/^INTERFACES.*/INTERFACES\=\"$interface\"/" /etc/default/isc-dhcp-server

echo '# A slightly different configuration for an internal subnet.' >> /etc/dhcp/dhcpd.conf
echo "subnet $idnet netmask $netmask {" >> /etc/dhcp/dhcpd.conf
echo "  range $initial $final;" >> /etc/dhcp/dhcpd.conf
echo "  option domain-name-servers $dnsip;" >> /etc/dhcp/dhcpd.conf
echo "  option domain-name \"$domain\";" >> /etc/dhcp/dhcpd.conf
echo "  option subnet-mask $netmask;" >> /etc/dhcp/dhcpd.conf
echo "  option routers $gateway;" >> /etc/dhcp/dhcpd.conf
echo "  option broadcast-address $broadcast;" >> /etc/dhcp/dhcpd.conf
echo "  default-lease-time 600;" >> /etc/dhcp/dhcpd.conf
echo "  max-lease-time 7200;" >> /etc/dhcp/dhcpd.conf
echo "}" >> /etc/dhcp/dhcpd.conf
clear

	read -p 'You need reserve ip for some host? y/n: ' fantasia
	#FANTASIA
	case $fantasia in
	'')
	cat commentsdhcp2 >> /etc/dhcp/dhcpd.conf
	service isc-dhcp-server restart
	clear
	echo 'IMPORTANT: If you this virtualizing remember change the network interface'
	echo 'from NAT to some local and them RESTART the service'
	exit
	;;
	yes|y|si|s)
	clear
	read -p "How many host wants to make a reservation? " nums

	while [ $nums -gt 0 ]
	do
	echo
	read -p "Please Enter the hostname: " host
	echo
	read -p "Please Enter the mac-address. eg; 08:00:07:26:c0:a5: " mac
	echo
	read -p "Please Enter the ip for the reservation: " ipre
	echo "host $host.$domain {" >> /etc/dhcp/dhcpd.conf
	echo "hardware ethernet $mac;" >> /etc/dhcp/dhcpd.conf
	echo "fixed-address $ipre;" >> /etc/dhcp/dhcpd.conf
	echo "}" >> /etc/dhcp/dhcpd.conf
	let nums-=1
	echo
	echo "$nums left"
	done

	cat commentsdhcp2 >> /etc/dhcp/dhcpd.conf
	service isc-dhcp-server restart
	clear
	echo 'IMPORTANT: If you this virtualizing remember change the network interface'
	echo 'from NAT to some local and them RESTART the service'
	exit
	;;



	no|n)
	cat commentsdhcp2 >> /etc/dhcp/dhcpd.conf
	service isc-dhcp-server restart
	clear
	echo 'IMPORTANT: If you this virtualizing remember change the network interface'
	echo 'from NAT to some local and them RESTART the service'
	exit
	;;
	*)exit
	cat commentsdhcp2 >> /etc/dhcp/dhcpd.conf
	service isc-dhcp-server restart
	clear
	echo 'IMPORTANT: If you this virtualizing remember change the network interface'
	echo 'from NAT to some local and them RESTART the service'
	exit
	;;
	esac
;;
u|unistall|desinstalar)
service isc-dhcp-server stop
rm -rf /etc/dhcp
sed -in "s/^INTERFACES.*/INTERFACES\=\"\"/" /etc/default/isc-dhcp-server
apt-get remove --purge isc-dhcp-server
;;
*)
exit
;;
esac
