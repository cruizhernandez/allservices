#!/bin/bash
clear
read -p "YOU WANT INSTALL OR UNISTALL i/u: " var
case $var in
'')
exit
;;
	i|install|inst|instalar)
	clear
	read -p "You want install a ldap-server o ldap-client? s/c -> " foo
	case $foo in
	'')
	exit
	;;
	s|server|servidor)
	clear
	apt-get -y install slapd ldap-utils ldap-auth-client nscd
	clear
	read -p "Please Enter your domain. eg: ina.net--> " domain
	echo
	read -p "Please Enter the address IP of this server--> " ipserv
	echo
	echo $domain > file.tmp
	cp /etc/ldap/ldap.conf /usr/ldap.conf
	dom=$(cat file.tmp | cut -d "." -f 1)
	inio=$(cat file.tmp | cut -d "." -f 2)
	echo "BASE	dc=$dom,dc=$inio" >> /etc/ldap/ldap.conf
	echo "URI	ldap://$ipserv" >> /etc/ldap/ldap.conf
	dpkg-reconfigure slapd
	clear
	echo "IMPORTANT--> If the network interface is in NAT the instalation"
	echo "does not work, CHANGE THE TTY and configure the network interface"
	read -p "MAKE SURE!!! and press ENTER to continue.." sdfsdfs
	clear
	echo "Aplying test";sleep 2
	ldapsearch -x
	read -p "Please press ENTER to continue.. " asdfa
	clear
	echo "Please wait..."
	mkdir /etc/ldap/estructures
	touch /etc/ldap/estructures/org.ldif
	touch /etc/ldap/estructures/contain.ldif
	#ORG.LDIF
	echo "dn: ou=usuario, dc=$dom, dc=$inio" > /etc/ldap/estructures/org.ldif
	echo "ou: people" >> /etc/ldap/estructures/org.ldif
	echo "objectClass: organizationalUnit" >> /etc/ldap/estructures/org.ldif
	echo '' >> /etc/ldap/estructures/org.ldif
	echo "dn: ou=grupo, dc=$dom, dc=$inio" >> /etc/ldap/estructures/org.ldif
	echo "ou: grupo" >> /etc/ldap/estructures/org.ldif
	echo "objectClass: organizationalUnit" >> /etc/ldap/estructures/org.ldif
	ldapadd -x -D cn=admin,dc=$dom,dc=$inio -W -f /etc/ldap/estructures/org.ldif
	read -p "Press ENTER to continue.." ss
	clear
	#CONTAIN.ORG
	echo dn: cn=ldapusers, ou=grupo, dc=$dom, dc=$inio > /etc/ldap/estructures/contain.ldif
	echo objectClass: posixGroup >> /etc/ldap/estructures/contain.ldif
	echo cn: ldapusers >> /etc/ldap/estructures/contain.ldif
	echo gidNumber: 3 >> /etc/ldap/estructures/contain.ldif
	ldapadd -x -D cn=admin,dc=$dom,dc=$inio -W -f /etc/ldap/estructures/contain.ldif
	read -p "Press ENTER to continue.." sss
	clear
	read -p "How many users you want add? " users
	echo
		while [ $users -gt 0 ]
		do
		clear
		read -p "Please Enter the user name: " username
		echo
		read -p "Please Enter the last name: " lastname
		echo
		read -p "Please Enter the UID Number [5-1500]: " idnumber
		echo
		echo "Plese Enter the user Password: "
		read -s pass
		echo
		read -p "Please Enter the home directory path. eg: /home/user " directory

		#USER.LDIF
		echo
		echo "dn: uid=$username, ou=usuario, dc=$dom, dc=$inio" > /etc/ldap/estructures/$username.ldif
		echo "objectClass:inetOrgPerson" >> /etc/ldap/estructures/$username.ldif
		echo "objectClass:posixAccount" >> /etc/ldap/estructures/$username.ldif
		echo "objectClass:person" >> /etc/ldap/estructures/$username.ldif
		echo "uid: $username" >> /etc/ldap/estructures/$username.ldif
		echo "sn: $lastname" >> /etc/ldap/estructures/$username.ldif
		echo "givenName: $username $lastname" >> /etc/ldap/estructures/$username.ldif
		echo "cn: $username $lastname" >> /etc/ldap/estructures/$username.ldif
		echo "displayName: $username$lastname" >> /etc/ldap/estructures/$username.ldif
		echo "uidNumber: $idnumber" >> /etc/ldap/estructures/$username.ldif
		echo "gidNumber: $idnumber" >> /etc/ldap/estructures/$username.ldif
		echo "userPassword: $pass" >> /etc/ldap/estructures/$username.ldif
		echo "homeDirectory: $directory" >> /etc/ldap/estructures/$username.ldif
		echo "loginShell: /bin/sh" >> /etc/ldap/estructures/$username.ldif
		echo "mail: $username@$domain" >> /etc/ldap/estructures/$username.ldif
		ldapadd -x -D cn=admin,dc=$dom,dc=$inio -W -f /etc/ldap/estructures/$username.ldif
		read -p "Press ENTER to continue.." ssss
		clear
		let users-=1
		echo "$users left"
		done
	rm -rf file.tmp
	auth-client-config -t nss -p  lac_ldap
	echo "required pam_mkhomedir.so umask=0022 skel=/etc/skel" >> /usr/share/pam-configs/mkhomedir
	echo "session required pam_mkhomedir.so" >> /etc/pam.d/common-session
	service nscd restart
	service slapd restart
	clear
	getent passwd
	echo "Script by: Cristhian Ruiz Hernandez TET03"
	;;
	c|client|cliente)
	clear
	apt-get -y install ldap-auth-client nscd
	auth-client-config -t nss -p  lac_ldap
	echo "required pam_mkhomedir.so umask=0022 skel=/etc/skel" >> /usr/share/pam-configs/mkhomedir
	echo "session required pam_mkhomedir.so" >> /etc/pam.d/common-session
	service nscd restart
	clear
	getent passwd
	echo "Script by: Cristhian Ruiz Hernandez TET03"
	;;
	*)
	exit
	;;
	esac
	;;

u|unistall|desinstalar)
clear
apt-get -y remove --purge slapd ldap-utils ldap-auth-client nscd
rm -rf /etc/ldap/estructures
cp /usr/ldap.conf /etc/ldap/ldap.conf
echo "Script by: Cristhian Ruiz Hernandez TET03"
;;

*)
exit
;;
esac
