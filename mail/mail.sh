#/bin/bash
clear
read -p "YOU WANT INSTALL OR UNISTALL THE MAIL SERVER i/u: " request
case $request in
'')
exit
;;
i|install|instalar|inst)
	apt-get -y install apache2 apache2-doc apache2-utils openssl postfix php5 dovecot-core dovecot-imapd dovecot-pop3d squirrelmail
	clear
	dpkg-reconfigure postfix
	service postfix restart
	service dovecot restart
	service apache2 restart
	clear
	read -p "Please Enter your domain. eg: ina.net--> " domain
	echo
	read -p "Please Enter the hostname or server mail alias--> " mail
	echo
	read -p "Please Enter the admin user name--> " admin
	echo
	cp /etc/apache2/sites-enabled/000-default.conf /etc/apache2/sites-enabled/mail.conf
	echo '<VirtualHost *:80>' > /etc/apache2/sites-enabled/mail.conf
	echo "	ServerName $mail.$domain" >> /etc/apache2/sites-enabled/mail.conf
	echo "	ServerAdmin $admin@$domain" >> /etc/apache2/sites-enabled/mail.conf
	echo "	DocumentRoot /var/www/squirrelmail" >> /etc/apache2/sites-enabled/mail.conf
	echo '	ErrorLog ${APACHE_LOG_DIR}/error.log' >> /etc/apache2/sites-enabled/mail.conf
	echo '	CustomLog ${APACHE_LOG_DIR}/access.log combined' >> /etc/apache2/sites-enabled/mail.conf
	echo '</VirtualHost>' >> /etc/apache2/sites-enabled/mail.conf
	ln -s /usr/share/squirrelmail /var/www/
	chmod 777 /var/mail
	service postfix restart
	service dovecot restart
	service apache2 restart
		clear
		echo "Instalation Successful. The installed server is postfix under"
		echo "Dovecot instalation. Also you installed squirrelmail like client mail"
		echo
		echo "If you are virtualizing REMEBER change the NIC configuration"
		echo "and restart: dovecot, apache and postfix"
		echo "Script by: Cristhian Ruiz Hernandez TET03"
;;
u|unistall|des|desinstalar)
clear
apt-get -y autoremove --purge postfix php5 dovecot-core dovecot-imapd dovecot-pop3d squirrelmail
clear
rm -rf /etc/postfix/main.cf
rm -rf /var/www/squirrelmail
echo "Script by: Cristhian Ruiz Hernandez TET03"
;;
*)
exit
;;
esac
